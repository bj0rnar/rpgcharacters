package herostrategy;

import model.BaseStats;

/**
 * Ranger strategy pattern
 */

public class RangerStatsStrategyManipulation implements IHeroStatsStrategy{
    //Increase stats based on static values when Hero level up
    @Override
    public void levelUp(BaseStats stats) {
        stats.addToStats(20,2,5,1);
    }

    //Set default BaseStats when Hero object is created
    @Override
    public BaseStats setBaseStats() {
        return new BaseStats(120, 5, 10, 2);
    }
}
