package herostrategy;

import model.BaseStats;

/**
 * Strategy pattern for setting default hero BaseStats and increasing BaseStats based on level
 */

public interface IHeroStatsStrategy {
    void levelUp(BaseStats stats);
    BaseStats setBaseStats();
}
