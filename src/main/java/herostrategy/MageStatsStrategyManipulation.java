package herostrategy;

import model.BaseStats;

/**
 * Mage Strategy pattern
 */


public class MageStatsStrategyManipulation implements IHeroStatsStrategy {
    //Increase stats based on static values when Hero level up
    @Override
    public void levelUp(BaseStats stats) {
        stats.addToStats(15, 1, 2, 5);
    }

    //Set default BaseStats when Hero object is created
    @Override
    public BaseStats setBaseStats() {
        return new BaseStats(100, 2, 3, 10);
    }
}
