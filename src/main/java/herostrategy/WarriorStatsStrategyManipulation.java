package herostrategy;

import model.BaseStats;

public class WarriorStatsStrategyManipulation implements IHeroStatsStrategy {
    //Increase stats based on static values when Hero level up
    @Override
    public void levelUp(BaseStats stats) {
        stats.addToStats(30, 5, 2, 1);
    }

    //Set default BaseStats when Hero object is created
    @Override
    public BaseStats setBaseStats() {
        return new BaseStats(150,10, 3, 1);
    }
}