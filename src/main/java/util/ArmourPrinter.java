package util;

import armour.Armour;

/**
 * Class for printing Armour variables to console
 */

public class ArmourPrinter {
    public static void printArmourToConsole(Armour armour){
        System.out.println("Name: " + armour.getArmourName());
        System.out.println(armour.getBaseStats());
        System.out.println("Lvl: " + armour.getLevel());

    }
}
