package util;

import heroes.Hero;
import model.Experience;

/**
 * Class for calculating levels gained by XP
 */


public class ExperienceCalculator {

    public static void gainLevelsBasedOnExperience(Hero hero, int xp) {
        while (true) {
            //if the incoming xp is less than or equal to the remaining xp hero needs to level
            if (xp >= (hero.getExperience().getXpRequiredToLevel()) - hero.getExperience().getXp()) {
                hero.getExperience().setLevel((hero.getExperience().getLevel() + 1));
                //Subtract incoming xp required from incoming xp
                xp = (xp - hero.getExperience().getXpRequiredToLevel());
                //add 10% to the level requiprement
                double temp = hero.getExperience().getXpRequiredToLevel() * 1.10;
                hero.getExperience().setXpRequiredToLevel((int) temp);
                //pump stats
                hero.getiHeroStatsStrategy().levelUp(hero.getHeroStats());
            } else {
                //Set the Hero objects xp to incoming + residiual xp
                hero.getExperience().setXp(hero.getExperience().getXp() + xp);
                break;
            }
        }
    }

}
