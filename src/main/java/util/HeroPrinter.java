package util;

import heroes.Hero;


/**
 * Class for printing Hero variables to console
 */

public class HeroPrinter {

    public static void printHeroToConsole(Hero hero){
        System.out.println(hero.getHeroType() + " details");
        System.out.println("Name: " + hero.getName());
        System.out.println(hero.getHeroStats().toString());
        System.out.println("Lvl: " + hero.getExperience().getLevel());
        System.out.println("Current xp: " + hero.getExperience().getXp());
        System.out.println("Required to level: " + hero.getExperience().getXpRequiredToLevel());
        System.out.println("Hero attack damage: " + hero.getDamage());
    }
}
