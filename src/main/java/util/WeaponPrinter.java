package util;

import heroes.Hero;
import weapon.Weapon;

/**
 * Class for printing Weapon variables to console
 */

public class WeaponPrinter {

    public static void printWeaponToConsole(Weapon weapon){
        System.out.println("Weapon name: " + weapon.getWeaponName());
        System.out.println("Weapon level: " + weapon.getWeaponLevel());
        System.out.println("Weapon type: " + weapon.getWeaponType());
        System.out.println("Weapon damage: " + weapon.getDamage());
    }
}
