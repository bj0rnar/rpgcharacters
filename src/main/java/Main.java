import armour.*;
import armourTypeStrategy.ClothArmour;
import armourTypeStrategy.LeatherArmour;
import armourTypeStrategy.PlateArmour;
import heroes.Hero;
//import heroes.Mage;
//import heroes.Ranger;
//import heroes.Warrior;
import heroes.HeroType;
import herostrategy.MageStatsStrategyManipulation;
import herostrategy.RangerStatsStrategyManipulation;
import herostrategy.WarriorStatsStrategyManipulation;
import itemequip.ArmourEquipper;
import itemequip.WeaponEquipper;
import model.Experience;
import util.ExperienceCalculator;
import weapon.Weapon;
import weapon.WeaponType;
import weaponstrategy.MeleeWeaponScaling;

import static util.HeroPrinter.printHeroToConsole;
import static util.ArmourPrinter.printArmourToConsole;
import static util.WeaponPrinter.printWeaponToConsole;


/**
 * TODO:
 * Check for SOLID breaches as there definitively are some in this chaos
 */


public class Main {
    public static void main(String[] args){


        Hero myHero = new Hero("Newbie", HeroType.WARRIOR, new WarriorStatsStrategyManipulation());
        ExperienceCalculator.gainLevelsBasedOnExperience(myHero, 50);
        ExperienceCalculator.gainLevelsBasedOnExperience(myHero, 100);
        ExperienceCalculator.gainLevelsBasedOnExperience(myHero, 110);

        printHeroToConsole(myHero);

        Armour plateChest = new ArmourBodySlot("Platechest of DOOOOOOOM", ArmourType.PLATE, 1, new PlateArmour());
        Armour clothHead = new ArmourHeadSlot("Fancy hat", ArmourType.CLOTH, 1, new ClothArmour());
        Armour leatherPants = new ArmourLegSlot("Stylish pants", ArmourType.LEATHER, 1, new LeatherArmour());
        ArmourEquipper.equipPieceOfArmour(myHero, plateChest);
        ArmourEquipper.equipPieceOfArmour(myHero, clothHead);
        ArmourEquipper.equipPieceOfArmour(myHero, leatherPants);

        Weapon handOfSulfuras = new Weapon("Hand of Sulfuras", WeaponType.MELEE, new MeleeWeaponScaling(), 1);
        WeaponEquipper.equipWeapon(myHero, handOfSulfuras);
        System.out.println("-------------DECKED--------------");
        printHeroToConsole(myHero);

        ArmourEquipper.unequipPieceOfArmour(myHero, plateChest);
        ArmourEquipper.unequipPieceOfArmour(myHero, clothHead);
        ArmourEquipper.unequipPieceOfArmour(myHero, leatherPants);

        WeaponEquipper.unequipWeapon(myHero, handOfSulfuras);

        System.out.println("-------------STRIPPED--------------");
        printHeroToConsole(myHero);


        /*
        System.out.println("---------------------------------------------");
        Weapon umbra = new Weapon("Umbra", WeaponType.MELEE, new MeleeWeaponScaling(), 1);
        printWeaponToConsole(umbra);
        WeaponEquipper.equipWeapon(myHero, umbra);
        System.out.println("---------------------------------------------");
        printHeroToConsole(myHero);
        WeaponEquipper.unequipWeapon(myHero, umbra);
        System.out.println("----------------------------------------------");
        printHeroToConsole(myHero);
        */

        //Hello from local git

        /*
        myHero.gainExperience(1200);
        System.out.println("-----------------------------------------------------------------------------------");
        printHeroToConsole(myHero);
        System.out.println("-----------------------------------------------------------------------------------");

        Hero mage = new Hero("Emma Stone", HeroType.MAGE, new MageStatsStrategyManipulation());
        mage.gainExperience(1555);
        printHeroToConsole(mage);
        System.out.println("-----------------------------------------------------------------------------------");

        Hero ranger = new Hero("SOLID SNAKE", HeroType.RANGER, new RangerStatsStrategyManipulation());
        ranger.gainExperience(2100);
        printHeroToConsole(ranger);
        System.out.println("------------------------------------------------------------------------------------");
        Armour plateArmour = new ArmourBodySlot("Kaom's Heart", ArmourType.PLATE, 15, new PlateArmour());
        printArmourToConsole(plateArmour);
        System.out.println("------------------------------------------------------------------------------------");
        Armour clothLegs = new ArmourLegSlot("A wizardly skirt", ArmourType.CLOTH, 10, new ClothArmour());
        printArmourToConsole(clothLegs);
        System.out.println("------------------------------------------------------------------------------------");
        Armour leatherChest = new ArmourBodySlot("Time to take a break", ArmourType.LEATHER, 10, new LeatherArmour());
        printArmourToConsole(leatherChest);
        System.out.println("------------------------------------------------------------------------------------");

         */



    }





}
