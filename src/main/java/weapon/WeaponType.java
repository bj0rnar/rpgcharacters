package weapon;

/**
 * Enum for setting type of Weapon
 */
public enum WeaponType {
    MELEE,
    RANGED,
    MAGIC
}
