package weapon;

import weaponstrategy.IWeaponScalingStrategy;

/**
 * Weapon class
 */

public class Weapon {

    private String weaponName;
    private WeaponType weaponType;
    private int damage;
    private int weaponLevel;
    private IWeaponScalingStrategy weaponScalingStrategy;

    //Generic Weapon constructor. Type for melee, ranged or magic and Strategy Pattern for handling weapon damage scaling
    public Weapon(String weaponName, WeaponType weaponType, IWeaponScalingStrategy weaponScalingStrategy, int weaponLevel) {
        this.weaponName = weaponName;
        this.weaponType = weaponType;
        this.weaponScalingStrategy = weaponScalingStrategy;
        this.damage = this.weaponScalingStrategy.setDamage(weaponLevel);
        this.weaponLevel = weaponLevel;
    }

    public String getWeaponName() {
        return weaponName;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public int getWeaponLevel() {
        return weaponLevel;
    }
}
