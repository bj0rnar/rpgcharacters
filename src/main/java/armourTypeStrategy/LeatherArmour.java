package armourTypeStrategy;

import model.BaseStats;

/**
 * Strategy pattern for cloth armour
 * Sets BaseStats to starting stats + scaling as items cannot level further
 */

public class LeatherArmour implements IArmourStrategy{
    @Override
    public BaseStats setBaseStats(int level) {
        return new BaseStats(20+(8*level), 2+(2*level), 1+(level), 0);
    }
}
