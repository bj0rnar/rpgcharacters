package armourTypeStrategy;

import model.BaseStats;

/**
 * Strategy pattern for setting and scaling the BaseStats (hp, str, dex, int)
 */

public interface IArmourStrategy {
    //Can handle scaling in one go as items cannot level up further
    BaseStats setBaseStats(int level);
}
