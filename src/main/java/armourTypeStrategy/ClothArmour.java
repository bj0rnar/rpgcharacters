package armourTypeStrategy;

import model.BaseStats;

/**
 * Strategy pattern for cloth armour
 * Sets BaseStats to starting stats + scaling as items cannot level further
 */


public class ClothArmour implements IArmourStrategy{
    @Override
    public BaseStats setBaseStats(int level) {
        return new BaseStats(10+(5*level), 0, 1+(level), 3+(2*level));
    }
}
