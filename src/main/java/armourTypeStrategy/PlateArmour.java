package armourTypeStrategy;

import model.BaseStats;

/**
 * Strategy pattern for cloth armour
 * Sets BaseStats to starting stats + scaling as items cannot level further
 */

public class PlateArmour implements IArmourStrategy {
    @Override
    public BaseStats setBaseStats(int level) {
        return new BaseStats(30 + (12*(level-1)), 3 + (2*(level-1)), 1+(level-1),0);
    }
}
