package armour;

/**
 * Used to define armour type in the Armour class.
 */

public enum ArmourType {
    CLOTH,
    LEATHER,
    PLATE
}
