package armour;

import armourTypeStrategy.IArmourStrategy;

/**
 * Using inheritence to set slot
 */

public class ArmourLegSlot extends Armour{
    public ArmourLegSlot(String armourName, ArmourType armourType, int level, IArmourStrategy iArmourStrategy) {
        super(armourName, armourType, level, iArmourStrategy);
        //Scales all the BaseStats (hp, str, dex, int) from armour down to 60%
        this.getBaseStats().scaleStatsWithArmour(0.60);
    }
}
