package armour;

import armourTypeStrategy.IArmourStrategy;
import model.BaseStats;

/**
 * Armour Class
 */

public abstract class Armour {

    private String armourName;
    private ArmourType armourType;
    private int level;
    private IArmourStrategy iArmourStrategy;
    private BaseStats baseStats;

    //Generic cloth, leather or plate Armour. Enum for defining type and ArmourStrategy for handling the scaling
    public Armour(String armourName, ArmourType armourType, int level, IArmourStrategy iArmourStrategy) {
        this.armourName = armourName;
        this.armourType = armourType;
        this.level = level;
        this.iArmourStrategy = iArmourStrategy;
        this.baseStats = this.iArmourStrategy.setBaseStats(this.level);
    }

    public ArmourType getArmourType() {
        return armourType;
    }

    public int getLevel() {
        return level;
    }

    public String getArmourName() {
        return armourName;
    }

    public BaseStats getBaseStats() {
        return baseStats;
    }
}
