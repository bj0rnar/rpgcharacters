package armour;

import armourTypeStrategy.IArmourStrategy;

/**
 * Using inheritence to set slot
 */

public class ArmourBodySlot extends Armour {
    public ArmourBodySlot(String armourName, ArmourType armourType, int level, IArmourStrategy iArmourStrategy) {
        super(armourName, armourType, level, iArmourStrategy);
    }
}
