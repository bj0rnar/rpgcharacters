package heroes;

import armour.ArmourBodySlot;
import armour.ArmourHeadSlot;
import armour.ArmourLegSlot;
import herostrategy.IHeroStatsStrategy;
import model.BaseStats;
import model.Experience;
import weapon.Weapon;

public class Hero {

    /**
     * Base class for all Hero object
     */

    //All related hero stats
    private final String name;
    private final HeroType heroType;
    private final IHeroStatsStrategy iHeroStatsStrategy;
    private final BaseStats baseStats;
    private final Experience experience = new Experience(1, 0, 100);
    private int damage;


    //Armour objects to represent the different slots
    private ArmourBodySlot currentBodyArmour;
    private ArmourLegSlot currentLegArmour;
    private ArmourHeadSlot currentHeadArmour;

    //Weapon object to represent.. well the weapon
    private Weapon weapon;

    //Constructor passing name, enumerator type and the strategy pattern to set BaseStats
    public Hero(String name, HeroType heroType, IHeroStatsStrategy iheroStatsStrategy) {
        this.name = name;
        this.heroType = heroType;
        this.iHeroStatsStrategy = iheroStatsStrategy;
        this.baseStats = iheroStatsStrategy.setBaseStats();
    }

    //Getters and setters
    public IHeroStatsStrategy getiHeroStatsStrategy() {
        return iHeroStatsStrategy;
    }

    public Experience getExperience() {
        return experience;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public ArmourBodySlot getCurrentBodyArmour() {
        return currentBodyArmour;
    }

    public void setCurrentBodyArmour(ArmourBodySlot currentBodyArmour) {
        this.currentBodyArmour = currentBodyArmour;
    }

    public ArmourLegSlot getCurrentLegArmour() {
        return currentLegArmour;
    }

    public void setCurrentLegArmour(ArmourLegSlot currentLegArmour) {
        this.currentLegArmour = currentLegArmour;
    }

    public ArmourHeadSlot getCurrentHeadArmour() {
        return currentHeadArmour;
    }

    public void setCurrentHeadArmour(ArmourHeadSlot currentHeadArmour) {
        this.currentHeadArmour = currentHeadArmour;
    }

    public HeroType getHeroType() { return heroType; }

    public BaseStats getHeroStats() {
        return baseStats;
    }

    public String getName() {
        return name;
    }
}
