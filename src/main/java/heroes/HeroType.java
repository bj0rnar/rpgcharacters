package heroes;

/**
 * Enumerator for the different classes instead of a hierarchy
 */

public enum HeroType {
    WARRIOR,
    RANGER,
    MAGE
}
