package model;

public class BaseStats {

    /**
     * Class for keeping track of the base stats
     */

    private int health;
    private int strength;
    private int dexterity;
    private int intelligence;

    public BaseStats(int health, int strength, int dexterity, int intelligence) {
        this.health = health;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    //Equipping / leveling logic
    public void addToStats(int health, int strength, int dexterity, int intelligence){
        this.health += health;
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    //Use this for equipping
    public void subtractFromStats(int health, int strength, int dexterity, int intelligence){
        this.health -= health;
        this.strength -= strength;
        this.dexterity -= dexterity;
        this.intelligence -= intelligence;
    }

    //Scaling with armour slot
    public void scaleStatsWithArmour(double percentage){
        this.health *= percentage;
        this.strength *= percentage;
        this.dexterity *= percentage;
        this.intelligence *= percentage;
    }

    public int getHealth() {
        return health;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public String toString() {
        return "HP: " + health +
                "\nStr: " + strength +
                "\nDex: " + dexterity +
                "\nInt: " + intelligence;
    }
}
