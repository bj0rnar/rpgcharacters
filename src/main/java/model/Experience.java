package model;

public class Experience {

    /**
     * Class for keeping track of experience and level
     */

    private int level;
    private int xp;
    private int xpRequiredToLevel;

    public Experience(int level, int xp, int xpRequiredToLevel) {
        this.level = level;
        this.xp = xp;
        this.xpRequiredToLevel = xpRequiredToLevel;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getXpRequiredToLevel() {
        return xpRequiredToLevel;
    }

    public void setXpRequiredToLevel(int xpRequiredToLevel) {
        this.xpRequiredToLevel = xpRequiredToLevel;
    }
}
