package weaponstrategy;

import weapon.WeaponType;

/**
 * Strategy pattern for setting the weapons inherent damage based on type and item level
 */
public interface IWeaponScalingStrategy {
    int setDamage(int itemLevel);
}
