package weaponstrategy;

import weapon.WeaponType;

/**
 * Damage scaling for Magic Weapon
 */
public class MagicWeaponScaling implements IWeaponScalingStrategy {
    //itemlevel-1 because itemlevel should start at one (like Hero), but it's important that level 1 receives no scaling (2 * 0 = 0)
    @Override
    public int setDamage(int itemLevel) {
        return 25 + (2 * (itemLevel-1));
    }
}
