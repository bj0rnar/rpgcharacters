package itemequip;

import heroes.Hero;
import weapon.Weapon;

public class WeaponEquipper {

    public static void equipWeapon(Hero hero, Weapon weapon){
        if (hero.getExperience().getLevel() >= weapon.getWeaponLevel()){
            if (hero.getWeapon() == null){
                switch (weapon.getWeaponType()) {
                    case MELEE -> {
                        hero.setWeapon(weapon);
                        ItemStatsCalculation.increaseHeroDamageWithWeapon(hero, weapon, hero.getHeroStats().getStrength(), 1.5);
                    }
                    case RANGED -> {
                        hero.setWeapon(weapon);
                        ItemStatsCalculation.increaseHeroDamageWithWeapon(hero, weapon, hero.getHeroStats().getDexterity(), 2);
                    }
                    case MAGIC -> {
                        hero.setWeapon(weapon);
                        ItemStatsCalculation.increaseHeroDamageWithWeapon(hero, weapon, hero.getHeroStats().getIntelligence(), 3);
                    }
                }
            }

        }
    }

    public static void unequipWeapon(Hero hero, Weapon weapon){
        if (hero.getWeapon() != null){
            hero.setWeapon(null);
            hero.setDamage(0);
        }
    }


}
