package itemequip;

import armour.*;
import heroes.Hero;
import model.BaseStats;
import weapon.Weapon;

/**
 * Class for handling equipping Armour and scaling the stats thereafter
 */

public class ArmourEquipper {

    public static void equipPieceOfArmour(Hero hero, Armour armour){
        //Hero must be higher or equal level in order to wear the armour
        if (hero.getExperience().getLevel() >= armour.getLevel()) {

            //Use polymorphism to check which instance of Armour subclass and set it
            if (armour instanceof ArmourBodySlot) {
                equipBodyArmour(hero, armour);
            }
            if (armour instanceof ArmourLegSlot) {
                equipLegArmour(hero, armour);
            }
            if (armour instanceof ArmourHeadSlot) {
                equipHeadArmour(hero, armour);
            }
        }
        else System.out.println(hero.getName() + " is too low level to equip that item");
    }

    private static void equipHeadArmour(Hero hero, Armour armour) {
        //Checks if head armour slot is already occupied
        if (hero.getCurrentHeadArmour() == null){
            hero.setCurrentHeadArmour((ArmourHeadSlot) armour);
            //Pass both objects to calculate stat gain
            ItemStatsCalculation.addArmourStatsToHero(hero, armour);
        }
        else {
            System.out.println(hero.getName() + " is already wearing head armour");
        }
    }

    private static void equipLegArmour(Hero hero, Armour armour) {
        if (hero.getCurrentLegArmour() == null){
            hero.setCurrentLegArmour((ArmourLegSlot) armour);
            ItemStatsCalculation.addArmourStatsToHero(hero, armour);
        }
    }

    private static void equipBodyArmour(Hero hero, Armour armour) {
        if (hero.getCurrentBodyArmour() == null) {
            hero.setCurrentBodyArmour((ArmourBodySlot) armour);
            ItemStatsCalculation.addArmourStatsToHero(hero, armour);
        }
    }

    public static void unequipPieceOfArmour(Hero hero, Armour armour){
        //Check which instance of Armour it is
        if (armour instanceof ArmourBodySlot){
            //Check if hero is not wearing
            if (hero.getCurrentBodyArmour() != null){
                hero.setCurrentBodyArmour(null);
                ItemStatsCalculation.subtractArmourStatsFromhero(hero, armour);
            }
        }
        if (armour instanceof ArmourHeadSlot){
            if (hero.getCurrentHeadArmour() != null){
                hero.setCurrentHeadArmour(null);
                ItemStatsCalculation.subtractArmourStatsFromhero(hero, armour);
            }
        }
        if (armour instanceof ArmourLegSlot){
            if (hero.getCurrentLegArmour() != null){
                hero.setCurrentLegArmour(null);
                ItemStatsCalculation.subtractArmourStatsFromhero(hero, armour);
            }
        }
    }



}
