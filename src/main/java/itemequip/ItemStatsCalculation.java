package itemequip;

import armour.Armour;
import heroes.Hero;
import model.BaseStats;
import weapon.Weapon;

/**
 * Class to add or subtract from BaseStats or Hero objects damage variable based on the weapon/armour equipped
 */

public class ItemStatsCalculation {

    public static void addArmourStatsToHero(Hero hero, Armour armour) {
        BaseStats armourStats = armour.getBaseStats();
        hero.getHeroStats().addToStats(armourStats.getHealth(), armourStats.getStrength(), armourStats.getDexterity(), armourStats.getIntelligence());
    }

    public static void subtractArmourStatsFromhero(Hero hero, Armour armour) {
        BaseStats armourStats = armour.getBaseStats();
        hero.getHeroStats().subtractFromStats(armourStats.getHealth(), armourStats.getStrength(), armourStats.getDexterity(), armourStats.getIntelligence());
    }

    public static void increaseHeroDamageWithWeapon(Hero hero, Weapon weapon, int mainStat, double scaling) {
        double calculatedDamage = (mainStat + weapon.getDamage()) * scaling;
        hero.setDamage((int) calculatedDamage);
    }
}
