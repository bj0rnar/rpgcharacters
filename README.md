# RPGCharacters

RPGCharacters is an backend system for creating characters, weapons and armour with scaling stats for all components. The system follows the SOLID principles and uses the strategy design pattern for handling different types of weapons/characters/armour.

## Motivation

This project was made as an assignment during the Experis Academy Java Fullstack course.

## Code example

All creation is done in the Main class

```java
//Creating characters
Hero myHero = new Hero("Hero name", HeroType.WARRIOR, new WarriorStatsStrategyManipulation());

//Experience
ExperienceCalculator.gainLevelsBasedOnExperience(myHero, 50);

//Creating armour
Armour myArmour= new ArmourBodySlot("Armour Name", ArmourType.PLATE, 1, new PlateArmour());

//Creating weapon
Weapon myWeapon = new Weapon("Weapon Name", WeaponType.MELEE, new MeleeWeaponScaling(), 1);

//Equipping armour and weapon
WeaponEquipper.equipWeapon(myHero, myWeapon);
ArmourEquipper.unequipPieceOfArmour(myHero, myArmour);
```

## Project tree
```bash
+---.idea
+---src
|   +---main
|   |   +---java
|   |   |   +---armour
|   |   |   +---armourTypeStrategy
|   |   |   +---heroes
|   |   |   +---herostrategy
|   |   |   +---itemequip
|   |   |   +---model
|   |   |   +---util
|   |   |   +---weapon
|   |   |   \---weaponstrategy
|   |   \---resources
|   \---test
|       \---java
\---target
    +---classes
    |   +---armour
    |   +---armourTypeStrategy
    |   +---heroes
    |   +---herostrategy
    |   +---itemequip
    |   +---model
    |   +---util
    |   +---weapon
    |   \---weaponstrategy
    +---generated-sources
    |   \---annotations
    +---generated-test-sources
    |   \---test-annotations
    \---test-classes
```

## Credits
Thanks to Nicholas Lennox for great guidance
